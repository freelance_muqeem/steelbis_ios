//
//  HomeViewController.swift
//  Steelbis
//
//  Created by Abdul Muqeem on 26/12/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
    }
    
    @IBOutlet weak var tabBar:UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "News"
        self.navigationController?.ShowNavigationBar()
        self.PlaceNavigationButtons(selectorForLeft: #selector(menuAction) , leftImage: MENU_IMAGE , selectorForRight: #selector(notificationAction) , rightImage: NOTIFICATION_IMAGE )
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        
        self.tabBar.delegate = self
        
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @objc func notificationAction() {
        self.showBanner(title: "Alert", subTitle: "Will be implemented", style: UIColor.BannerStyle.warning)
        return
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBar.items?[0].image = UIImage(named: "News_ icon _Green")?.withRenderingMode(.alwaysOriginal)
        
    }
    
}

extension HomeViewController : UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            return
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: PricesViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = PricesViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: VesselViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = VesselViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ForexViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ForexViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: B2BViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = B2BViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
    }
}
