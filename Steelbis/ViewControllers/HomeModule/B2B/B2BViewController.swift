//
//  B2BViewController.swift
//  Steelbis
//
//  Created by Abdul Muqeem on 01/01/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import UIKit

class B2BViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> B2BViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! B2BViewController
    }
    
    @IBOutlet weak var tabBar:UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "B2B"
        self.navigationController?.ShowNavigationBar()
        self.PlaceNavigationButtons(selectorForLeft: #selector(menuAction) , leftImage: MENU_IMAGE , selectorForRight: #selector(notificationAction) , rightImage: NOTIFICATION_IMAGE )
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        
        self.tabBar.delegate = self

    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @objc func notificationAction() {
        self.showBanner(title: "Alert", subTitle: "Will be implemented", style: UIColor.BannerStyle.warning)
        return
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBar.items?[4].image = UIImage(named: "B2B _Green _Icon")?.withRenderingMode(.alwaysOriginal)
    }

    
}

extension B2BViewController : UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: HomeViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: PricesViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = PricesViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: VesselViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = VesselViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ForexViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ForexViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            return
        }
    }
}
