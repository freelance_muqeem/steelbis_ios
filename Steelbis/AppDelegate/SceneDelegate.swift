//
//  SceneDelegate.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 30/10/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        self.window = window
        self.navigateToViewController()
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        
    }
    
    @available(iOS 13.0, *)
    static func getInstatnce() -> SceneDelegate {
        let scene = UIApplication.shared.connectedScenes.first
        return scene?.delegate as! SceneDelegate
    }
    
    func navigateToViewController() {
        
        if UserManager.isUserLogin() {
            
            let nav = RootViewController.instantiateFromStoryboard()
            self.window?.rootViewController = nav
            let vc = HomeViewController.instantiateFromStoryboard()
            nav.pushViewController(vc, animated: true)
            
        } else {
            
            let controller = SideMenuRootViewController.instantiateFromStoryboard()
            controller.leftViewPresentationStyle = .slideAbove
            self.window?.rootViewController = controller
            
        }
        
    }
    
}

