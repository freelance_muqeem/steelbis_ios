//
//  Data.swift
//
//  Created by Abdul Muqeem on 19/11/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class SocialLogin : NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let showEmail = "showEmail"
    static let redirectForm = "redirectForm"
    static let successMessage = "successMessage"
  }

  // MARK: Properties
  public var showEmail: Bool? = false
  public var redirectForm: Bool? = false
  public var successMessage: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    showEmail = json[SerializationKeys.showEmail].boolValue
    redirectForm = json[SerializationKeys.redirectForm].boolValue
    successMessage = json[SerializationKeys.successMessage].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.showEmail] = showEmail
    dictionary[SerializationKeys.redirectForm] = redirectForm
    if let value = successMessage { dictionary[SerializationKeys.successMessage] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.showEmail = aDecoder.decodeBool(forKey: SerializationKeys.showEmail)
    self.redirectForm = aDecoder.decodeBool(forKey: SerializationKeys.redirectForm)
    self.successMessage = aDecoder.decodeObject(forKey: SerializationKeys.successMessage) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(showEmail, forKey: SerializationKeys.showEmail)
    aCoder.encode(redirectForm, forKey: SerializationKeys.redirectForm)
    aCoder.encode(successMessage, forKey: SerializationKeys.successMessage)
  }

}
