//
//  Data.swift
//
//  Created by Abdul Muqeem on 01/11/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Countries: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let nationalities = "nationalities"
    static let successMessage = "successMessage"
  }

  // MARK: Properties
  public var nationalities: [String]?
  public var successMessage: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.nationalities].array { nationalities = items.map { $0.stringValue } }
    successMessage = json[SerializationKeys.successMessage].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = nationalities { dictionary[SerializationKeys.nationalities] = value }
    if let value = successMessage { dictionary[SerializationKeys.successMessage] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.nationalities = aDecoder.decodeObject(forKey: SerializationKeys.nationalities) as? [String]
    self.successMessage = aDecoder.decodeObject(forKey: SerializationKeys.successMessage) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(nationalities, forKey: SerializationKeys.nationalities)
    aCoder.encode(successMessage, forKey: SerializationKeys.successMessage)
  }

}
