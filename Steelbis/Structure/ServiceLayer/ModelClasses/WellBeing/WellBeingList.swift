//
//  Teachersboards.swift
//
//  Created by Abdul Muqeem on 26/11/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class WellBeingList: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let isActive = "isActive"
    static let id = "_id"
    static let posterType = "posterType"
    static let postId = "postId"
    static let postPictureUrl = "postPictureUrl"
    static let v = "__v"
    static let postText = "postText"
    static let postedBy = "postedBy"
    static let postTitle = "postTitle"
    static let postTimestamp = "postTimestamp"
    static let category = "category"
  }

  // MARK: Properties
  public var isActive: Bool? = false
  public var id: String?
  public var posterType: Int?
  public var postId: Int?
  public var postPictureUrl: String?
  public var v: Int?
  public var postText: String?
  public var postedBy: String?
  public var postTitle: String?
  public var postTimestamp: String?
  public var category: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    isActive = json[SerializationKeys.isActive].boolValue
    id = json[SerializationKeys.id].string
    posterType = json[SerializationKeys.posterType].int
    postId = json[SerializationKeys.postId].int
    postPictureUrl = json[SerializationKeys.postPictureUrl].string
    v = json[SerializationKeys.v].int
    postText = json[SerializationKeys.postText].string
    postedBy = json[SerializationKeys.postedBy].string
    postTitle = json[SerializationKeys.postTitle].string
    postTimestamp = json[SerializationKeys.postTimestamp].string
    category = json[SerializationKeys.category].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.isActive] = isActive
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = posterType { dictionary[SerializationKeys.posterType] = value }
    if let value = postId { dictionary[SerializationKeys.postId] = value }
    if let value = postPictureUrl { dictionary[SerializationKeys.postPictureUrl] = value }
    if let value = v { dictionary[SerializationKeys.v] = value }
    if let value = postText { dictionary[SerializationKeys.postText] = value }
    if let value = postedBy { dictionary[SerializationKeys.postedBy] = value }
    if let value = postTitle { dictionary[SerializationKeys.postTitle] = value }
    if let value = postTimestamp { dictionary[SerializationKeys.postTimestamp] = value }
    if let value = category { dictionary[SerializationKeys.category] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.isActive = aDecoder.decodeBool(forKey: SerializationKeys.isActive)
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.posterType = aDecoder.decodeObject(forKey: SerializationKeys.posterType) as? Int
    self.postId = aDecoder.decodeObject(forKey: SerializationKeys.postId) as? Int
    self.postPictureUrl = aDecoder.decodeObject(forKey: SerializationKeys.postPictureUrl) as? String
    self.v = aDecoder.decodeObject(forKey: SerializationKeys.v) as? Int
    self.postText = aDecoder.decodeObject(forKey: SerializationKeys.postText) as? String
    self.postedBy = aDecoder.decodeObject(forKey: SerializationKeys.postedBy) as? String
    self.postTitle = aDecoder.decodeObject(forKey: SerializationKeys.postTitle) as? String
    self.postTimestamp = aDecoder.decodeObject(forKey: SerializationKeys.postTimestamp) as? String
    self.category = aDecoder.decodeObject(forKey: SerializationKeys.category) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(isActive, forKey: SerializationKeys.isActive)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(posterType, forKey: SerializationKeys.posterType)
    aCoder.encode(postId, forKey: SerializationKeys.postId)
    aCoder.encode(postPictureUrl, forKey: SerializationKeys.postPictureUrl)
    aCoder.encode(v, forKey: SerializationKeys.v)
    aCoder.encode(postText, forKey: SerializationKeys.postText)
    aCoder.encode(postedBy, forKey: SerializationKeys.postedBy)
    aCoder.encode(postTitle, forKey: SerializationKeys.postTitle)
    aCoder.encode(postTimestamp, forKey: SerializationKeys.postTimestamp)
    aCoder.encode(category, forKey: SerializationKeys.category)
  }

}
