//
//  Offers.swift
//
//  Created by Abdul Muqeem on 26/11/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class OffersList : NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let isActive = "isActive"
    static let offerDescription = "offerDescription"
    static let users = "users"
    static let offerCategory = "offerCategory"
    static let offerGallery = "offerGallery"
    static let isApproved = "isApproved"
    static let isFree = "isFree"
    static let offerSuccessMessage = "offerSuccessMessage"
    static let location = "location"
    static let offerName = "offerName"
    static let creationTimestamp = "creationTimestamp"
    static let id = "_id"
    static let offerTermsAndConditions = "offerTermsAndConditions"
    static let v = "__v"
    static let locationText = "locationText"
    static let vendorId = "vendorId"
    static let offerDiscount = "offerDiscount"
    static let offerId = "offerId"
    static let except = "except"
    static let offerPictureUrl = "offerPictureUrl"
  }

  // MARK: Properties
  public var isActive: Bool? = false
  public var offerDescription: String?
  public var users: [Any]?
  public var offerCategory: String?
  public var offerGallery: [String]?
  public var isApproved: Bool? = false
  public var isFree: Bool? = false
  public var offerSuccessMessage: String?
  public var location: String?
  public var offerName: String?
  public var creationTimestamp: String?
  public var id: String?
  public var offerTermsAndConditions: String?
  public var v: Int?
  public var locationText: String?
  public var vendorId: String?
  public var offerDiscount: String?
  public var offerId: Int?
  public var except: String?
  public var offerPictureUrl: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    isActive = json[SerializationKeys.isActive].boolValue
    offerDescription = json[SerializationKeys.offerDescription].string
    if let items = json[SerializationKeys.users].array { users = items.map { $0.object} }
    offerCategory = json[SerializationKeys.offerCategory].string
    if let items = json[SerializationKeys.offerGallery].array { offerGallery = items.map { $0.stringValue } }
    isApproved = json[SerializationKeys.isApproved].boolValue
    isFree = json[SerializationKeys.isFree].boolValue
    offerSuccessMessage = json[SerializationKeys.offerSuccessMessage].string
    location = json[SerializationKeys.location].string
    offerName = json[SerializationKeys.offerName].string
    creationTimestamp = json[SerializationKeys.creationTimestamp].string
    id = json[SerializationKeys.id].string
    offerTermsAndConditions = json[SerializationKeys.offerTermsAndConditions].string
    v = json[SerializationKeys.v].int
    locationText = json[SerializationKeys.locationText].string
    vendorId = json[SerializationKeys.vendorId].string
    offerDiscount = json[SerializationKeys.offerDiscount].string
    offerId = json[SerializationKeys.offerId].int
    except = json[SerializationKeys.except].string
    offerPictureUrl = json[SerializationKeys.offerPictureUrl].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.isActive] = isActive
    if let value = offerDescription { dictionary[SerializationKeys.offerDescription] = value }
    if let value = users { dictionary[SerializationKeys.users] = value }
    if let value = offerCategory { dictionary[SerializationKeys.offerCategory] = value }
    if let value = offerGallery { dictionary[SerializationKeys.offerGallery] = value }
    dictionary[SerializationKeys.isApproved] = isApproved
    dictionary[SerializationKeys.isFree] = isFree
    if let value = offerSuccessMessage { dictionary[SerializationKeys.offerSuccessMessage] = value }
    if let value = location { dictionary[SerializationKeys.location] = value }
    if let value = offerName { dictionary[SerializationKeys.offerName] = value }
    if let value = creationTimestamp { dictionary[SerializationKeys.creationTimestamp] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = offerTermsAndConditions { dictionary[SerializationKeys.offerTermsAndConditions] = value }
    if let value = v { dictionary[SerializationKeys.v] = value }
    if let value = locationText { dictionary[SerializationKeys.locationText] = value }
    if let value = vendorId { dictionary[SerializationKeys.vendorId] = value }
    if let value = offerDiscount { dictionary[SerializationKeys.offerDiscount] = value }
    if let value = offerId { dictionary[SerializationKeys.offerId] = value }
    if let value = except { dictionary[SerializationKeys.except] = value }
    if let value = offerPictureUrl { dictionary[SerializationKeys.offerPictureUrl] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.isActive = aDecoder.decodeBool(forKey: SerializationKeys.isActive)
    self.offerDescription = aDecoder.decodeObject(forKey: SerializationKeys.offerDescription) as? String
    self.users = aDecoder.decodeObject(forKey: SerializationKeys.users) as? [Any]
    self.offerCategory = aDecoder.decodeObject(forKey: SerializationKeys.offerCategory) as? String
    self.offerGallery = aDecoder.decodeObject(forKey: SerializationKeys.offerGallery) as? [String]
    self.isApproved = aDecoder.decodeBool(forKey: SerializationKeys.isApproved)
    self.isFree = aDecoder.decodeBool(forKey: SerializationKeys.isFree)
    self.offerSuccessMessage = aDecoder.decodeObject(forKey: SerializationKeys.offerSuccessMessage) as? String
    self.location = aDecoder.decodeObject(forKey: SerializationKeys.location) as? String
    self.offerName = aDecoder.decodeObject(forKey: SerializationKeys.offerName) as? String
    self.creationTimestamp = aDecoder.decodeObject(forKey: SerializationKeys.creationTimestamp) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.offerTermsAndConditions = aDecoder.decodeObject(forKey: SerializationKeys.offerTermsAndConditions) as? String
    self.v = aDecoder.decodeObject(forKey: SerializationKeys.v) as? Int
    self.locationText = aDecoder.decodeObject(forKey: SerializationKeys.locationText) as? String
    self.vendorId = aDecoder.decodeObject(forKey: SerializationKeys.vendorId) as? String
    self.offerDiscount = aDecoder.decodeObject(forKey: SerializationKeys.offerDiscount) as? String
    self.offerId = aDecoder.decodeObject(forKey: SerializationKeys.offerId) as? Int
    self.except = aDecoder.decodeObject(forKey: SerializationKeys.except) as? String
    self.offerPictureUrl = aDecoder.decodeObject(forKey: SerializationKeys.offerPictureUrl) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(isActive, forKey: SerializationKeys.isActive)
    aCoder.encode(offerDescription, forKey: SerializationKeys.offerDescription)
    aCoder.encode(users, forKey: SerializationKeys.users)
    aCoder.encode(offerCategory, forKey: SerializationKeys.offerCategory)
    aCoder.encode(offerGallery, forKey: SerializationKeys.offerGallery)
    aCoder.encode(isApproved, forKey: SerializationKeys.isApproved)
    aCoder.encode(isFree, forKey: SerializationKeys.isFree)
    aCoder.encode(offerSuccessMessage, forKey: SerializationKeys.offerSuccessMessage)
    aCoder.encode(location, forKey: SerializationKeys.location)
    aCoder.encode(offerName, forKey: SerializationKeys.offerName)
    aCoder.encode(creationTimestamp, forKey: SerializationKeys.creationTimestamp)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(offerTermsAndConditions, forKey: SerializationKeys.offerTermsAndConditions)
    aCoder.encode(v, forKey: SerializationKeys.v)
    aCoder.encode(locationText, forKey: SerializationKeys.locationText)
    aCoder.encode(vendorId, forKey: SerializationKeys.vendorId)
    aCoder.encode(offerDiscount, forKey: SerializationKeys.offerDiscount)
    aCoder.encode(offerId, forKey: SerializationKeys.offerId)
    aCoder.encode(except, forKey: SerializationKeys.except)
    aCoder.encode(offerPictureUrl, forKey: SerializationKeys.offerPictureUrl)
  }

}
