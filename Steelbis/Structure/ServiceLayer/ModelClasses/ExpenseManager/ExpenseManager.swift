//
//  Data.swift
//
//  Created by Abdul Muqeem on 04/12/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ExpenseManager: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let totalExpense = "totalExpense"
    static let outstandingBalance = "outstandingBalance"
    static let totalIncome = "totalIncome"
    static let income = "income"
    static let expenses = "expenses"
  }

  // MARK: Properties
  public var totalExpense: Int?
  public var outstandingBalance: Int?
  public var totalIncome: Int?
  public var income: [Income]?
  public var expenses: [Expenses]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    totalExpense = json[SerializationKeys.totalExpense].int
    outstandingBalance = json[SerializationKeys.outstandingBalance].int
    totalIncome = json[SerializationKeys.totalIncome].int
    if let items = json[SerializationKeys.income].array { income = items.map { Income(json: $0) } }
    if let items = json[SerializationKeys.expenses].array { expenses = items.map { Expenses(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = totalExpense { dictionary[SerializationKeys.totalExpense] = value }
    if let value = outstandingBalance { dictionary[SerializationKeys.outstandingBalance] = value }
    if let value = totalIncome { dictionary[SerializationKeys.totalIncome] = value }
    if let value = income { dictionary[SerializationKeys.income] = value.map { $0.dictionaryRepresentation() } }
    if let value = expenses { dictionary[SerializationKeys.expenses] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.totalExpense = aDecoder.decodeObject(forKey: SerializationKeys.totalExpense) as? Int
    self.outstandingBalance = aDecoder.decodeObject(forKey: SerializationKeys.outstandingBalance) as? Int
    self.totalIncome = aDecoder.decodeObject(forKey: SerializationKeys.totalIncome) as? Int
    self.income = aDecoder.decodeObject(forKey: SerializationKeys.income) as? [Income]
    self.expenses = aDecoder.decodeObject(forKey: SerializationKeys.expenses) as? [Expenses]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(totalExpense, forKey: SerializationKeys.totalExpense)
    aCoder.encode(outstandingBalance, forKey: SerializationKeys.outstandingBalance)
    aCoder.encode(totalIncome, forKey: SerializationKeys.totalIncome)
    aCoder.encode(income, forKey: SerializationKeys.income)
    aCoder.encode(expenses, forKey: SerializationKeys.expenses)
  }

}
