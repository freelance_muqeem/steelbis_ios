//
//  ActivitiesHistory.swift
//
//  Created by Abdul Muqeem on 28/11/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ActivitiesHistory: NSCoding {

      // MARK: Declaration for string constants to be used to decode and also serialize.
      private struct SerializationKeys {
        static let offerPictureUrl = "offerPictureUrl"
        static let offerName = "offerName"
        static let id = "id"
        static let offerId = "offerId"
        static let eventPictureUrl = "eventPictureUrl"
        static let buyEventId = "buyEventId"
        static let title = "title"
        static let timestamp = "timestamp"
        static let isAttended = "isAttended"
        static let activityType = "activityType"
        static let eventTimestamp = "eventTimestamp"
      }

      // MARK: Properties
      public var offerPictureUrl: String?
      public var offerName: String?
      public var id: Int?
      public var offerId: String?
      public var eventPictureUrl: String?
      public var buyEventId: String?
      public var title: String?
      public var timestamp: String?
      public var isAttended: Bool? = false
      public var activityType: String?
      public var eventTimestamp: String?

      // MARK: SwiftyJSON Initializers
      /// Initiates the instance based on the object.
      ///
      /// - parameter object: The object of either Dictionary or Array kind that was passed.
      /// - returns: An initialized instance of the class.
      public convenience init(object: Any) {
        self.init(json: JSON(object))
      }

      /// Initiates the instance based on the JSON that was passed.
      ///
      /// - parameter json: JSON object from SwiftyJSON.
      public required init(json: JSON) {
        offerPictureUrl = json[SerializationKeys.offerPictureUrl].string
        offerName = json[SerializationKeys.offerName].string
        id = json[SerializationKeys.id].int
        offerId = json[SerializationKeys.offerId].string
        eventPictureUrl = json[SerializationKeys.eventPictureUrl].string
        buyEventId = json[SerializationKeys.buyEventId].string
        title = json[SerializationKeys.title].string
        timestamp = json[SerializationKeys.timestamp].string
        isAttended = json[SerializationKeys.isAttended].boolValue
        activityType = json[SerializationKeys.activityType].string
        eventTimestamp = json[SerializationKeys.eventTimestamp].string
      }

      /// Generates description of the object in the form of a NSDictionary.
      ///
      /// - returns: A Key value pair containing all valid values in the object.
      public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = offerPictureUrl { dictionary[SerializationKeys.offerPictureUrl] = value }
        if let value = offerName { dictionary[SerializationKeys.offerName] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = offerId { dictionary[SerializationKeys.offerId] = value }
        if let value = eventPictureUrl { dictionary[SerializationKeys.eventPictureUrl] = value }
        if let value = buyEventId { dictionary[SerializationKeys.buyEventId] = value }
        if let value = title { dictionary[SerializationKeys.title] = value }
        if let value = timestamp { dictionary[SerializationKeys.timestamp] = value }
        dictionary[SerializationKeys.isAttended] = isAttended
        if let value = activityType { dictionary[SerializationKeys.activityType] = value }
        if let value = eventTimestamp { dictionary[SerializationKeys.eventTimestamp] = value }
        return dictionary
      }

      // MARK: NSCoding Protocol
      required public init(coder aDecoder: NSCoder) {
        self.offerPictureUrl = aDecoder.decodeObject(forKey: SerializationKeys.offerPictureUrl) as? String
        self.offerName = aDecoder.decodeObject(forKey: SerializationKeys.offerName) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.offerId = aDecoder.decodeObject(forKey: SerializationKeys.offerId) as? String
        self.eventPictureUrl = aDecoder.decodeObject(forKey: SerializationKeys.eventPictureUrl) as? String
        self.buyEventId = aDecoder.decodeObject(forKey: SerializationKeys.buyEventId) as? String
        self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
        self.timestamp = aDecoder.decodeObject(forKey: SerializationKeys.timestamp) as? String
        self.isAttended = aDecoder.decodeBool(forKey: SerializationKeys.isAttended)
        self.activityType = aDecoder.decodeObject(forKey: SerializationKeys.activityType) as? String
        self.eventTimestamp = aDecoder.decodeObject(forKey: SerializationKeys.eventTimestamp) as? String
      }

      public func encode(with aCoder: NSCoder) {
        aCoder.encode(offerPictureUrl, forKey: SerializationKeys.offerPictureUrl)
        aCoder.encode(offerName, forKey: SerializationKeys.offerName)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(offerId, forKey: SerializationKeys.offerId)
        aCoder.encode(eventPictureUrl, forKey: SerializationKeys.eventPictureUrl)
        aCoder.encode(buyEventId, forKey: SerializationKeys.buyEventId)
        aCoder.encode(title, forKey: SerializationKeys.title)
        aCoder.encode(timestamp, forKey: SerializationKeys.timestamp)
        aCoder.encode(isAttended, forKey: SerializationKeys.isAttended)
        aCoder.encode(activityType, forKey: SerializationKeys.activityType)
        aCoder.encode(eventTimestamp, forKey: SerializationKeys.eventTimestamp)
      }

    }
