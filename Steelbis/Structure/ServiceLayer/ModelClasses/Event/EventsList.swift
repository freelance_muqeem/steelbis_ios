//
//  Events.swift
//
//  Created by Abdul Muqeem on 26/11/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class EventsList : NSCoding {

      // MARK: Declaration for string constants to be used to decode and also serialize.
      private struct SerializationKeys {
        static let isActive = "isActive"
        static let hostType = "hostType"
        static let confirmationPin = "confirmationPin"
        static let descriptionValue = "description"
        static let creatorType = "creatorType"
        static let eventId = "eventId"
        static let price = "price"
        static let approvalTimestamp = "approvalTimestamp"
        static let hostedBy = "hostedBy"
        static let isApproved = "isApproved"
        static let eventLocation = "eventLocation"
        static let id = "_id"
        static let creationTimestamp = "creationTimestamp"
        static let eventPictureUrl = "eventPictureUrl"
        static let eventCategory = "eventCategory"
        static let v = "__v"
        static let attendanceCapacity = "attendanceCapacity"
        static let title = "title"
        static let approvedBy = "approvedBy"
        static let createdBy = "createdBy"
        static let eventTimestamp = "eventTimestamp"
        static let attendees = "attendees"
      }

      // MARK: Properties
      public var isActive: Bool? = false
      public var hostType: Int?
      public var confirmationPin: String?
      public var descriptionValue: String?
      public var creatorType: Int?
      public var eventId: Int?
      public var price: Int?
      public var approvalTimestamp: String?
      public var hostedBy: String?
      public var isApproved: Bool? = false
      public var eventLocation: String?
      public var id: String?
      public var creationTimestamp: String?
      public var eventPictureUrl: String?
      public var eventCategory: String?
      public var v: Int?
      public var attendanceCapacity: String?
      public var title: String?
      public var approvedBy: String?
      public var createdBy: String?
      public var eventTimestamp: String?
      public var attendees: [String]?

      // MARK: SwiftyJSON Initializers
      /// Initiates the instance based on the object.
      ///
      /// - parameter object: The object of either Dictionary or Array kind that was passed.
      /// - returns: An initialized instance of the class.
      public convenience init(object: Any) {
        self.init(json: JSON(object))
      }

      /// Initiates the instance based on the JSON that was passed.
      ///
      /// - parameter json: JSON object from SwiftyJSON.
      public required init(json: JSON) {
        isActive = json[SerializationKeys.isActive].boolValue
        hostType = json[SerializationKeys.hostType].int
        confirmationPin = json[SerializationKeys.confirmationPin].string
        descriptionValue = json[SerializationKeys.descriptionValue].string
        creatorType = json[SerializationKeys.creatorType].int
        eventId = json[SerializationKeys.eventId].int
        price = json[SerializationKeys.price].int
        approvalTimestamp = json[SerializationKeys.approvalTimestamp].string
        hostedBy = json[SerializationKeys.hostedBy].string
        isApproved = json[SerializationKeys.isApproved].boolValue
        eventLocation = json[SerializationKeys.eventLocation].string
        id = json[SerializationKeys.id].string
        creationTimestamp = json[SerializationKeys.creationTimestamp].string
        eventPictureUrl = json[SerializationKeys.eventPictureUrl].string
        eventCategory = json[SerializationKeys.eventCategory].string
        v = json[SerializationKeys.v].int
        attendanceCapacity = json[SerializationKeys.attendanceCapacity].string
        title = json[SerializationKeys.title].string
        approvedBy = json[SerializationKeys.approvedBy].string
        createdBy = json[SerializationKeys.createdBy].string
        eventTimestamp = json[SerializationKeys.eventTimestamp].string
        if let items = json[SerializationKeys.attendees].array { attendees = items.map { $0.stringValue } }
      }

      /// Generates description of the object in the form of a NSDictionary.
      ///
      /// - returns: A Key value pair containing all valid values in the object.
      public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary[SerializationKeys.isActive] = isActive
        if let value = hostType { dictionary[SerializationKeys.hostType] = value }
        if let value = confirmationPin { dictionary[SerializationKeys.confirmationPin] = value }
        if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
        if let value = creatorType { dictionary[SerializationKeys.creatorType] = value }
        if let value = eventId { dictionary[SerializationKeys.eventId] = value }
        if let value = price { dictionary[SerializationKeys.price] = value }
        if let value = approvalTimestamp { dictionary[SerializationKeys.approvalTimestamp] = value }
        if let value = hostedBy { dictionary[SerializationKeys.hostedBy] = value }
        dictionary[SerializationKeys.isApproved] = isApproved
        if let value = eventLocation { dictionary[SerializationKeys.eventLocation] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = creationTimestamp { dictionary[SerializationKeys.creationTimestamp] = value }
        if let value = eventPictureUrl { dictionary[SerializationKeys.eventPictureUrl] = value }
        if let value = eventCategory { dictionary[SerializationKeys.eventCategory] = value }
        if let value = v { dictionary[SerializationKeys.v] = value }
        if let value = attendanceCapacity { dictionary[SerializationKeys.attendanceCapacity] = value }
        if let value = title { dictionary[SerializationKeys.title] = value }
        if let value = approvedBy { dictionary[SerializationKeys.approvedBy] = value }
        if let value = createdBy { dictionary[SerializationKeys.createdBy] = value }
        if let value = eventTimestamp { dictionary[SerializationKeys.eventTimestamp] = value }
        if let value = attendees { dictionary[SerializationKeys.attendees] = value }
        return dictionary
      }

      // MARK: NSCoding Protocol
      required public init(coder aDecoder: NSCoder) {
        self.isActive = aDecoder.decodeBool(forKey: SerializationKeys.isActive)
        self.hostType = aDecoder.decodeObject(forKey: SerializationKeys.hostType) as? Int
        self.confirmationPin = aDecoder.decodeObject(forKey: SerializationKeys.confirmationPin) as? String
        self.descriptionValue = aDecoder.decodeObject(forKey: SerializationKeys.descriptionValue) as? String
        self.creatorType = aDecoder.decodeObject(forKey: SerializationKeys.creatorType) as? Int
        self.eventId = aDecoder.decodeObject(forKey: SerializationKeys.eventId) as? Int
        self.price = aDecoder.decodeObject(forKey: SerializationKeys.price) as? Int
        self.approvalTimestamp = aDecoder.decodeObject(forKey: SerializationKeys.approvalTimestamp) as? String
        self.hostedBy = aDecoder.decodeObject(forKey: SerializationKeys.hostedBy) as? String
        self.isApproved = aDecoder.decodeBool(forKey: SerializationKeys.isApproved)
        self.eventLocation = aDecoder.decodeObject(forKey: SerializationKeys.eventLocation) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
        self.creationTimestamp = aDecoder.decodeObject(forKey: SerializationKeys.creationTimestamp) as? String
        self.eventPictureUrl = aDecoder.decodeObject(forKey: SerializationKeys.eventPictureUrl) as? String
        self.eventCategory = aDecoder.decodeObject(forKey: SerializationKeys.eventCategory) as? String
        self.v = aDecoder.decodeObject(forKey: SerializationKeys.v) as? Int
        self.attendanceCapacity = aDecoder.decodeObject(forKey: SerializationKeys.attendanceCapacity) as? String
        self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
        self.approvedBy = aDecoder.decodeObject(forKey: SerializationKeys.approvedBy) as? String
        self.createdBy = aDecoder.decodeObject(forKey: SerializationKeys.createdBy) as? String
        self.eventTimestamp = aDecoder.decodeObject(forKey: SerializationKeys.eventTimestamp) as? String
        self.attendees = aDecoder.decodeObject(forKey: SerializationKeys.attendees) as? [String]
      }

      public func encode(with aCoder: NSCoder) {
        aCoder.encode(isActive, forKey: SerializationKeys.isActive)
        aCoder.encode(hostType, forKey: SerializationKeys.hostType)
        aCoder.encode(confirmationPin, forKey: SerializationKeys.confirmationPin)
        aCoder.encode(descriptionValue, forKey: SerializationKeys.descriptionValue)
        aCoder.encode(creatorType, forKey: SerializationKeys.creatorType)
        aCoder.encode(eventId, forKey: SerializationKeys.eventId)
        aCoder.encode(price, forKey: SerializationKeys.price)
        aCoder.encode(approvalTimestamp, forKey: SerializationKeys.approvalTimestamp)
        aCoder.encode(hostedBy, forKey: SerializationKeys.hostedBy)
        aCoder.encode(isApproved, forKey: SerializationKeys.isApproved)
        aCoder.encode(eventLocation, forKey: SerializationKeys.eventLocation)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(creationTimestamp, forKey: SerializationKeys.creationTimestamp)
        aCoder.encode(eventPictureUrl, forKey: SerializationKeys.eventPictureUrl)
        aCoder.encode(eventCategory, forKey: SerializationKeys.eventCategory)
        aCoder.encode(v, forKey: SerializationKeys.v)
        aCoder.encode(attendanceCapacity, forKey: SerializationKeys.attendanceCapacity)
        aCoder.encode(title, forKey: SerializationKeys.title)
        aCoder.encode(approvedBy, forKey: SerializationKeys.approvedBy)
        aCoder.encode(createdBy, forKey: SerializationKeys.createdBy)
        aCoder.encode(eventTimestamp, forKey: SerializationKeys.eventTimestamp)
        aCoder.encode(attendees, forKey: SerializationKeys.attendees)
      }

    }
