//
//  Data.swift
//
//  Created by Abdul Muqeem on 26/11/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class EventCategories: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let eventCategories = "eventCategories"
  }

  // MARK: Properties
  public var eventCategories: [String]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.eventCategories].array { eventCategories = items.map { $0.stringValue } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = eventCategories { dictionary[SerializationKeys.eventCategories] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.eventCategories = aDecoder.decodeObject(forKey: SerializationKeys.eventCategories) as? [String]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(eventCategories, forKey: SerializationKeys.eventCategories)
  }

}
