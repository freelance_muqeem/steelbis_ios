//
//  UserServices.swift
//  Food
//
//  Created by Abdul Muqeem on 24/10/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserServices {
    
    static func Login(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.login , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
        
    static func FacebookLogin(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.fblogin , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func GoogleLogin(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.googlelogin , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func Register(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.register , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    static func FBRegister(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.fbRegister , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    static func GoogleRegister(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.googleRegister , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    static func Activate(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.activate , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    static func ResendToken(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.resendToken , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    static func ForgotPassword(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.forgotPassword , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    static func ResetPassword(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.resetPassword , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    static func Country(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.countryList , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }

    
}

