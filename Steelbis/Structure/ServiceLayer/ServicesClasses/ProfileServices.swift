//
//  ProfileServices.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 06/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import Foundation
import SwiftyJSON

class ProfileServices {
    
    static func GetProfile(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.getProfile , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func UpdateProfile(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.updateProfile , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func UpdateProfileImage(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.updateProfileImage , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func VerifyAccount(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.verification , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func UpdateSmartPass(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.updateSmartPass , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func UpdatePassword(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.updatePassword , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func InviteFriend(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.inviteFriend , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func SubmitRelocation(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.relocation , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func Connect(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.feedback , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func AccountClosed(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.closeAccount , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func About(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.getAbout , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func Terms(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.getTerms , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func GetMyActivities(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.getActivities , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func GetSubscription(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.getSubscription , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func GetMonthYear(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.getMonth , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func GetExpenseSheet(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.getexpenseSheet , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func AddIncome(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.addIncome , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func AddExpense(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.addExpense , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }

    static func EditIncome(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.editIncome , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }

    static func EditExpense(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.editExpense , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }

    static func GetOfferMessage(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.getOfferMessage , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
}
