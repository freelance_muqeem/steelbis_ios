//
//  DropDown.swift
//  Lahore
//
//  Created by Huda Jawed on 8/2/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import DropDown

extension DropDown
{
    static func configureDropDown(dropDown: DropDown, view : AnchorView)
    {
        dropDown.dismissMode = .onTap
        dropDown.anchorView = view
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.backgroundColor = UIColor.white
        dropDown.textFont = UIFont(name: "HelveticaNeue", size: 15.0)!
      //  dropDown.
        dropDown.cellHeight = 33
    }
}
