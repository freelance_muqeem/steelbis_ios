//
//  UIColorExtension.swift
//  HireMile
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    convenience init(rgb: UInt) {
        
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func randomFloat() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
    
    static func random() -> UIColor {
        return UIColor(red:   UIColor.randomFloat(),
                       green: UIColor.randomFloat(),
                       blue:  UIColor.randomFloat(),
                       alpha: 1.0)
    }
    
    struct BannerStyle {
        
        static var danger : UIColor  {
            return UIColor.init(rgb: 0xff0000)
        }
        
        static var success : UIColor {
            return UIColor.init(rgb: 0x008000)
        }
        
        static var warning : UIColor {
            return UIColor.init(rgb: 0xffd700)
        }
        
    }
    
}


extension UIView
{
    func setGradientBackground(colorTop: UIColor, colorBottom: UIColor)
    {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
