//
//  AppDelegate.swift
//  ParkingOnTheGo
//
//  Created by Minhasoft on 4/24/18.
//  Copyright © 2018 Minhasoft. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    
    func TransparentNavigationBar() {
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.isTranslucent = true
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.backgroundColor = UIColor.clear
        setNavigationBarHidden(false, animated: true)
    }
    
    func ThemedNavigationBar() {
        self.navigationBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        self.navigationBar.isTranslucent = false
    }
    
    func ChangeNavigationBarColor(color: UIColor) {
        self.navigationBar.barTintColor = color
        self.navigationBar.isTranslucent = false
    }
    
    func HideNavigationBar() {
        self.isNavigationBarHidden = true
    }
    
    func ShowNavigationBar() {
        self.isNavigationBarHidden = false
    }
    
    func ChangeTitleColor(color: UIColor) {
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: color]
    }
    
    func ChangeTitleFont() {
         self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,
         NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Medium", size: 19)!]
    }
    
}

