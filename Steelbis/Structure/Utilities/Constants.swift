//
//  Constants.swift
//  HireMile
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import UIKit

public let THEME_COLOR: UInt = 0xB0CD5C
public let DARK_THEME_COLOR: UInt = 0x97B642

public let BACK_IMAGE: UIImage = UIImage(named:"back_icon")!
public let MENU_IMAGE: UIImage = UIImage(named:"menu_icon")!
public let SHARE_IMAGE: UIImage = UIImage(named:"share_icon")!
public let NOTIFICATION_IMAGE: UIImage = UIImage(named:"notification_icon")!

// Alert
public let SUCCESS_IMAGE: UIImage = UIImage(named:"success")!
public let FAILURE_IMAGE: UIImage = UIImage(named:"error")!

let USERUPDATED = "UserUpdated"
var VERIFIED = "Verified"
var SUBMITTED = "Submitted"
var SUBSCRIPTION = "Subscription"

@available(iOS 13.0, *)
let SCENE_DELEGATE              = UIApplication.shared.delegate as! SceneDelegate
let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
let UIWINDOW                    = UIApplication.shared.delegate!.window!

// Extra

public let User_data_userDefault   = "User_data_userDefault"
public let token_userDefault   = "token_userDefault"
public let NOTIFICATION_USERUPDATE = "user_update"


